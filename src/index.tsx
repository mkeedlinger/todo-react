import React from "react";
import ReactDOM from "react-dom";
import * as Firebase from "firebase/app";
require("firebase/firestore");
import EventEmitter from "events";
import * as Firebaseui from "firebaseui";

Firebase.initializeApp({
  apiKey: "AIzaSyD-C1C7I7yCGzRVKz6R3NTM9zusXUrPfnQ",
  authDomain: "what-todo-100.firebaseapp.com",
  databaseURL: "https://what-todo-100.firebaseio.com",
  projectId: "what-todo-100",
  storageBucket: "what-todo-100.appspot.com",
  messagingSenderId: "161483411020",
  appId: "1:161483411020:web:a1ebe0a010669f25622ee9"
});

const loginEventHandler = new EventEmitter();
const db = Firebase.firestore().collection("user-lists");

var ui = new Firebaseui.auth.AuthUI(Firebase.auth());
ui.start("#auth", {
  signInOptions: [Firebase.auth.GoogleAuthProvider.PROVIDER_ID],
  signInSuccessUrl: "https://what-todo-100.firebaseapp.com/",
  callbacks: {
    signInSuccessWithAuthResult: (_authResult): boolean => {
      Firebase.auth().setPersistence(Firebase.auth.Auth.Persistence.LOCAL);
      loginEventHandler.emit("login");
      document.querySelector("#auth").remove();
      return false;
    }
  }
});

Firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    loginEventHandler.emit("login");
    document.querySelector("#auth").remove();
  } else {
    loginEventHandler.emit("logout");
  }
});

interface Item {
  description: string;
  done: boolean;
}

interface TaskListState {
  items: Item[];
}

class TaskList extends React.Component<any, TaskListState> {
  constructor(props) {
    super(props);

    this.state = {
      items: []
    };

    loginEventHandler.on("login", () => {
      let currentUser = getCurrentUid();
      console.log(`watching for changes: ${currentUser}`);
      db.doc(getCurrentUid()).onSnapshot(doc => {
        console.log("Current data: ", doc.data());
        this.setState({
          ...this.state,
          items: doc.data().items as Item[]
        });
      });
      getUserItems().then(items => {
        this.setState({
          items
        });
      });
    });

    loginEventHandler.on("logout", () => {
      this.setState({
        items: []
      });
    });

    if (Firebase.auth().currentUser !== null) {
      getUserItems().then(items => {
        this.setState({
          items
        });
      });
    }
  }
  newItem = () => {
    let items = this.state.items.slice();
    items = items.filter(item => item.description.length !== 0);
    items.unshift({
      description: "",
      done: false
    });
    this.setState({
      ...this.state,
      items
    });
  };
  render() {
    return (
      <div className="vertical-center-parent">
        <div>
          <h1>Todo</h1>
          <p>A super simple todo!</p>
        </div>
        <div>
          <button onClick={this.newItem} className="new-task-button">
            ➕ New Task
          </button>
        </div>
        <div>{this.listItems()}</div>
      </div>
    );
  }
  onInput(index) {
    return (event: React.ChangeEvent<HTMLInputElement>) => {
      let items = this.state.items.slice();
      items[index] = {
        ...items[index],
        description: event.target.value
      };
      this.setState({ ...this.state, items });
      db.doc(getCurrentUid()).set({
        items
      });
    };
  }
  onCheck(index) {
    return () => {
      let items = this.state.items.slice();
      items[index] = {
        ...items[index],
        done: !items[index].done
      };
      this.setState({ ...this.state, items });
      db.doc(getCurrentUid()).set({
        items
      });
    };
  }
  listItems() {
    return this.state.items.map((item, index) => {
      return (
        <TaskListItem
          key={index}
          item={item}
          onDelete={() => {
            let items = this.state.items.slice();
            items.splice(index, 1);
            this.setState({
              ...this.state,
              items: items
            });
            db.doc(getCurrentUid()).set({
              items
            });
          }}
          onCheck={this.onCheck(index)}
          onInput={this.onInput(index)}
        ></TaskListItem>
      );
    });
  }
}

interface TaskListItemProps {
  item: Item;
  onCheck: () => void;
  onInput: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onDelete: () => void;
}

class TaskListItem extends React.Component<TaskListItemProps> {
  render() {
    return (
      <div className="task-list-item">
        <input
          type="checkbox"
          checked={this.props.item.done}
          onChange={this.props.onCheck}
          className="task-done"
        ></input>
        <input
          type="text"
          placeholder="What do you want to do?"
          value={this.props.item.description}
          onChange={this.props.onInput}
        ></input>
        <button onClick={this.props.onDelete} className="delete-button">
          ❌
        </button>
      </div>
    );
  }
}

function getUserItems(): Promise<Item[]> {
  const docRef = db.doc(getCurrentUid());
  return docRef.get().then(
    (doc): Promise<Item[]> => {
      if (doc.exists) {
        let items = doc.data().items as Item[];
        return Promise.resolve(items);
      } else {
        return doc.ref.set([]).then(() => {
          return Promise.resolve([] as Item[]);
        });
      }
    }
  );
}

function getCurrentUid(): string {
  return Firebase.auth().currentUser.uid;
}

ReactDOM.render(<TaskList></TaskList>, document.getElementById("root"));
