# Simple React Todo!

[CLICK ME](https://what-todo-100.firebaseapp.com/) and see what I can do!

## What can I do?

- Persistence

  When signed in, tasks are immediatly saved as they are edited.

- Realtime editing

  Try opening the task-list in two windows and making changes. Your changes will move between tabs in real time!

- Account Creation

  Signing in with Google makes you a simple account immedietly. Signing out is as simple as closing the tab. Sign in again and your tasks will immediatly load!

## How am I built?

### Frontend libraries
- React
- Firebase
- Sass CSS langauge
- Events (brings the node EventEmitter class to the frontend)
- Typescript (thank goodness for types)

### Backend tools
- Firebase CLI
- Parcel Bundler

## How do I build this?
It should be as simple as:

```bash
npm install
npx parcel build src/index.html
# open dist/index.html
```

## Anything else to say?
Yeah. I think this works as a great proof-of-concept. There are a few things
missing, but that is somewhat on purpose and this was meant to prove my
abilities and not great an actual final product.
